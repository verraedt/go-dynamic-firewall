module gitlab.com/verraedt/go-dynamic-firewall

go 1.16

require (
	github.com/42wim/ipsetd v0.0.0-20170427154912-326ec0e331c0
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/digitalocean/godo v1.15.0
	github.com/hashicorp/consul/api v1.12.0
	github.com/kr/pty v1.1.4 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.0.0
	github.com/tehnerd/gnl2go v0.0.0-20161218223753-101b5c6e2d44
	github.com/vishvananda/netlink v1.0.0
	github.com/vishvananda/netns v0.0.0-20180720170159-13995c7128cc // indirect
	golang.org/x/oauth2 v0.0.0-20190523182746-aaccbc9213b0
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
	gopkg.in/alecthomas/kingpin.v2 v2.2.6 // indirect
)
