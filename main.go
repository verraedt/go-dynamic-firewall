package main

import (
	"context"
	"os"
	"os/signal"

	"github.com/spf13/cobra"
	"gitlab.com/verraedt/go-dynamic-firewall/firewall"

	log "github.com/sirupsen/logrus"
)

func main() {
	var (
		config firewall.Config
		debug  bool
	)

	rootCmd := &cobra.Command{
		Use:   "dynamic-firewall",
		Short: "Update the dynamic firewall ipsets and load balancer rules",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if debug {
				log.SetLevel(log.DebugLevel)
			}

			c := make(chan os.Signal, 1)
			signal.Notify(c, os.Interrupt)

			f, err := firewall.New(context.Background(), config)
			if err != nil {
				log.Fatal(err)
			}

			go func() {
				<-c
				f.Close()
			}()

			err = f.Run()
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	rootCmd.Flags().BoolVarP(&config.Loop, "wait", "d", false, "Wait for changes in consul and apply them immediately (daemon mode)")
	rootCmd.Flags().StringVar(&config.ConsulAddr, "consul", "http://consul.service.consul:8500", "Address of the consul service")
	rootCmd.Flags().BoolVarP(&debug, "debug", "v", false, "Verbose output for debugging")

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
