# github.com/42wim/ipsetd v0.0.0-20170427154912-326ec0e331c0
## explicit
github.com/42wim/ipsetd/ipset
# github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
## explicit
# github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
## explicit
# github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da
github.com/armon/go-metrics
# github.com/digitalocean/godo v1.15.0
## explicit
github.com/digitalocean/godo
# github.com/fatih/color v1.9.0
github.com/fatih/color
# github.com/golang/protobuf v1.3.1
github.com/golang/protobuf/proto
# github.com/google/go-querystring v1.0.0
github.com/google/go-querystring/query
# github.com/hashicorp/consul/api v1.12.0
## explicit
github.com/hashicorp/consul/api
# github.com/hashicorp/go-cleanhttp v0.5.1
github.com/hashicorp/go-cleanhttp
# github.com/hashicorp/go-hclog v0.12.0
github.com/hashicorp/go-hclog
# github.com/hashicorp/go-immutable-radix v1.0.0
github.com/hashicorp/go-immutable-radix
# github.com/hashicorp/go-rootcerts v1.0.2
github.com/hashicorp/go-rootcerts
# github.com/hashicorp/golang-lru v0.5.0
github.com/hashicorp/golang-lru/simplelru
# github.com/hashicorp/serf v0.9.6
github.com/hashicorp/serf/coordinate
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/konsorten/go-windows-terminal-sequences v1.0.1
github.com/konsorten/go-windows-terminal-sequences
# github.com/kr/pty v1.1.4
## explicit
github.com/kr/pty
# github.com/mattn/go-colorable v0.1.6
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.12
github.com/mattn/go-isatty
# github.com/mitchellh/go-homedir v1.1.0
github.com/mitchellh/go-homedir
# github.com/mitchellh/mapstructure v1.1.2
github.com/mitchellh/mapstructure
# github.com/sirupsen/logrus v1.2.0
## explicit
github.com/sirupsen/logrus
# github.com/spf13/cobra v1.0.0
## explicit
github.com/spf13/cobra
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/tehnerd/gnl2go v0.0.0-20161218223753-101b5c6e2d44
## explicit
github.com/tehnerd/gnl2go
# github.com/vishvananda/netlink v1.0.0
## explicit
github.com/vishvananda/netlink
github.com/vishvananda/netlink/nl
# github.com/vishvananda/netns v0.0.0-20180720170159-13995c7128cc
## explicit
github.com/vishvananda/netns
# golang.org/x/crypto v0.0.0-20190923035154-9ee001bba392
golang.org/x/crypto/ssh/terminal
# golang.org/x/net v0.0.0-20210410081132-afb366fc7cd1
golang.org/x/net/context
golang.org/x/net/context/ctxhttp
# golang.org/x/oauth2 v0.0.0-20190523182746-aaccbc9213b0
## explicit
golang.org/x/oauth2
golang.org/x/oauth2/internal
# golang.org/x/sys v0.0.0-20210330210617-4fbd30eecc44
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
## explicit
golang.org/x/time/rate
# google.golang.org/appengine v1.4.0
google.golang.org/appengine/internal
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/remote_api
google.golang.org/appengine/internal/urlfetch
google.golang.org/appengine/urlfetch
# gopkg.in/alecthomas/kingpin.v2 v2.2.6
## explicit
