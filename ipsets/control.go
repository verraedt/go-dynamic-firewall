package ipsets

import (
	"fmt"
	"sync"

	"github.com/42wim/ipsetd/ipset"
)

type Set map[IPType]*Ipset

type Control struct {
	Sets   map[string]Set
	client *ipset.IPset
	sync.Mutex
}

func New() *Control {
	return &Control{
		Sets: map[string]Set{},
	}
}

func (c *Control) Set(name, definition string, timeout uint) Set {
	if set, ok := c.Sets[name]; ok {
		return set
	}

	c.Sets[name] = Set{}

	c.Sets[name][IPv4] = NewIpset(IpsetConfig{
		Ipset:              name,
		Type:               definition,
		Family:             IPv4,
		Timeout:            timeout,
		IpsetClientFactory: c.IpsetClientFactory,
	})

	c.Sets[name][IPv6] = NewIpset(IpsetConfig{
		Ipset:              fmt.Sprintf("%s-6", name),
		Type:               definition,
		Family:             IPv6,
		Timeout:            timeout,
		IpsetClientFactory: c.IpsetClientFactory,
	})

	return c.Sets[name]
}

// IpsetClientFactory callback.
func (c *Control) IpsetClientFactory(err error) (*ipset.IPset, error) {
	c.Lock()
	defer c.Unlock()

	if err == nil && c.client != nil {
		return c.client, nil
	}

	c.client, err = DefaultIpsetClientFactory(err)

	return c.client, err
}

// Update a set with new entries.
func (s Set) Update(entries []IpsetEntry) error {
	for _, ipset := range s {
		if err := ipset.Update(entries); err != nil {
			return err
		}
	}

	return nil
}
