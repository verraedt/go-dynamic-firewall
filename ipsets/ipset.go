package ipsets

import (
	"fmt"
	"net"
	"sync"

	"github.com/42wim/ipsetd/ipset"

	log "github.com/sirupsen/logrus"
)

var IpsetPath = "/sbin/ipset"

// IPType specifies the type of an ip address.
type IPType string

const (
	// IPv4 addresses
	IPv4 IPType = "inet"
	// IPv6 addresses
	IPv6 IPType = "inet6"
)

// GetIPType gets IPType of net.IP.
func GetIPType(ip net.IP) IPType {
	if ip.To4() != nil {
		return IPv4
	}

	return IPv6
}

// An IpsetEntry describes an entry to be added to some ipset.
type IpsetEntry struct {
	Family  IPType
	Addr    string
	Timeout uint
}

func (e *IpsetEntry) String() string {
	if e.Timeout > 0 {
		return fmt.Sprintf("%s timeout %d", e.Addr, e.Timeout)
	}

	return e.Addr
}

// IpsetConfig is configuration for an Ipset handler.
type IpsetConfig struct {
	Ipset              string
	Type               string
	Family             IPType
	Timeout            uint
	IpsetClientFactory func(error) (*ipset.IPset, error)
}

// A Ipset handler.
type Ipset struct {
	IpsetConfig
}

// DefaultIpsetClientFactory is the default IpsetClientFactory.
var DefaultIpsetClientFactory = func(error) (*ipset.IPset, error) {
	return ipset.NewIPset(IpsetPath), nil
}

// NewIpset prepares an Ipset handler.
func NewIpset(config IpsetConfig) *Ipset {
	if config.IpsetClientFactory == nil {
		config.IpsetClientFactory = DefaultIpsetClientFactory
	}

	return &Ipset{
		IpsetConfig: config,
	}
}

// Update an ipset with new entries.
func (s *Ipset) Update(entries []IpsetEntry) error {
	if err := s.prepare(); err != nil {
		return err
	}

	for _, entry := range entries {
		if entry.Family != s.Family {
			// Skip invalid type
			continue
		}

		if err := s.add(entry); err != nil {
			return err
		}
	}

	return s.commit()
}

func (s *Ipset) prepare() error {
	if err := s.cmd(fmt.Sprintf("destroy %s-__NEW__\n", s.Ipset)); err != nil {
		return err
	}

	var cmd string

	if s.Timeout != 0 {
		cmd = fmt.Sprintf("create %s-__NEW__ %s family %s timeout %d\n", s.Ipset, s.Type, s.Family, s.Timeout)
	} else {
		cmd = fmt.Sprintf("create %s-__NEW__ %s family %s\n", s.Ipset, s.Type, s.Family)
	}

	return s.cmd(cmd)
}

var globalIpsetLock = sync.Mutex{}

func (s *Ipset) cmd(cmd string) error {
	globalIpsetLock.Lock()
	defer globalIpsetLock.Unlock()

	client, err := s.IpsetClientFactory(nil)
	if err != nil {
		return err
	}

	out, err := client.Cmd(cmd)
	if err != nil {
		log.Printf("error %s occurred during %s, restarting ipset client", err, cmd)

		// Recover by starting new ipset process
		client, err = s.IpsetClientFactory(err)
		if err != nil {
			return err
		}

		out, err = client.Cmd(cmd)
	}

	log.Debugf("ipset: '%s' returned '%s'", cmd, out)

	return err
}

func (s *Ipset) add(entry IpsetEntry) error {
	var cmd string

	if s.Timeout != 0 && entry.Timeout != 0 {
		cmd = fmt.Sprintf("add -exist %s-__NEW__ %s timeout %d\n", s.Ipset, entry.Addr, entry.Timeout)
	} else {
		cmd = fmt.Sprintf("add -exist %s-__NEW__ %s\n", s.Ipset, entry.Addr)
	}

	return s.cmd(cmd)
}

func (s *Ipset) commit() error {
	if err := s.cmd(fmt.Sprintf("create -exist %s %s family %s\n", s.Ipset, s.Type, s.Family)); err != nil {
		return err
	}

	if err := s.cmd(fmt.Sprintf("swap %s %s-__NEW__\n", s.Ipset, s.Ipset)); err != nil {
		return err
	}

	return s.cmd(fmt.Sprintf("destroy %s-__NEW__\n", s.Ipset))
}
