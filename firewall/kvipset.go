package firewall

import (
	"strconv"
	"strings"
	"time"
)

// KVIpsetEntry is a entry to be set in an ipset.
type KVIpsetEntry struct {
	Address    string   `json:"ip"`
	Name       string   `json:"name"`
	Expiration jsonTime `json:"expiration"`
	Effective  jsonTime `json:"effective"`
}

type jsonTime time.Time

func (t jsonTime) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(t).Unix(), 10)), nil
}

func (t *jsonTime) UnmarshalJSON(s []byte) (err error) {
	r := strings.ReplaceAll(string(s), `"`, ``)

	q, err := strconv.ParseInt(r, 10, 64)
	if err != nil {
		return err
	}

	*(*time.Time)(t) = time.Unix(q, 0)

	return
}

func (t jsonTime) Time() time.Time {
	return time.Time(t)
}

func (t jsonTime) String() string {
	return time.Time(t).String()
}
