package firewall

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/hashicorp/consul/api"
	"gitlab.com/verraedt/go-dynamic-firewall/ipsets"

	log "github.com/sirupsen/logrus"
)

type Input struct {
	Services     []*api.CatalogService // Consul services
	IpsetKVPairs []*api.KVPair         // Consul KV pairs with ipset information
}

type Output struct {
	DynamicInternal []ipsets.IpsetEntry            // Ipset DYNAMIC-INTERNAL
	DynamicExternal []ipsets.IpsetEntry            // Ipset DYNAMIC-EXTERNAL
	Ipsets          map[string][]ipsets.IpsetEntry // Additional uafw ipsets
}

// Compute the firewall state from the inputs
func (input *Input) Compute() (*Output, error) {
	output := &Output{
		Ipsets: map[string][]ipsets.IpsetEntry{},
	}

	for _, consulService := range input.Services {
		si, err := parseService(consulService)
		if err != nil {
			return nil, err
		}

		entries := createIpsetEntries(si.IPs, si.Protocol, si.Port, 0)

		if si.Internal {
			output.DynamicInternal = append(output.DynamicInternal, entries...)
		}

		if si.External {
			output.DynamicExternal = append(output.DynamicExternal, entries...)
		}
	}

	for _, kp := range input.IpsetKVPairs {
		parts := strings.Split(kp.Key, "/")
		name := strings.ToUpper(parts[len(parts)-1])

		if name == "" {
			continue
		}

		ipsetEntries, err := parseKVPair(kp)
		if err != nil {
			log.Warnf("Could not parse ipset entries for %s: %s. Ignoring.", name, err)
			continue
		}

		output.Ipsets[name] = ipsetEntries
	}

	return output, nil
}

type serviceInfo struct {
	IPs                    []net.IP // IPs of the service
	LBIPs                  []net.IP // Load balance ips to publish service on
	LBPublicIPs            bool     // Whether to include public ips for the load balancer
	Protocol               string
	Port                   uint16
	LB, Internal, External bool
	LBPublishOn            string
}

// parseService parses ipsets from a service registered in consul.
func parseService(service *api.CatalogService) (*serviceInfo, error) {
	info := &serviceInfo{
		Protocol: "tcp",
		Port:     uint16(service.ServicePort),
	}

	for _, tag := range service.ServiceTags {
		switch tag {
		case "udp":
			info.Protocol = "udp"
		case "fw.internal":
			info.Internal = true
		case "fw.external":
			info.External = true
		}
	}

	var address string
	if service.ServiceAddress != "" {
		address = service.ServiceAddress
	} else {
		address = service.Address
	}

	var err error

	info.IPs, err = parseAddress(address)
	if err != nil {
		return nil, err
	}

	return info, nil
}

func parseAddress(address string) ([]net.IP, error) {
	if address == "" {
		return nil, nil
	}

	if ip := net.ParseIP(address); ip != nil {
		return []net.IP{ip}, nil
	}

	return net.LookupIP(address)
}

func createIpsetEntries(ips []net.IP, protocol string, port uint16, timeout uint) []ipsets.IpsetEntry {
	entries := []ipsets.IpsetEntry{}

	for _, ip := range ips {
		entries = append(entries, ipsets.IpsetEntry{
			Family:  ipsets.GetIPType(ip),
			Addr:    fmt.Sprintf("%s,%s:%d", ip, protocol, port),
			Timeout: timeout,
		})
	}

	return entries
}

var ErrInvalidAddress = errors.New("invalid address")

func parseKVPair(kp *api.KVPair) ([]ipsets.IpsetEntry, error) {
	var (
		entries = []ipsets.IpsetEntry{}
		now     = time.Now()
		data    []*KVIpsetEntry
	)

	if err := json.Unmarshal(kp.Value, &data); err != nil {
		return nil, err
	}

	for _, dataEntry := range data {
		var (
			expires = time.Time(dataEntry.Expiration)
			starts  = time.Time(dataEntry.Effective)
		)

		if (!expires.IsZero() && now.After(expires)) || (!starts.IsZero() && now.Before(starts)) {
			continue
		}

		var timeout uint = 86400

		if !expires.IsZero() {
			timeout = uint(expires.Sub(now).Seconds())
		}

		if timeout > 86400 {
			timeout = 86400
		}

		address := dataEntry.Address

		if dataEntry.Name != "" {
			address = dataEntry.Name
		}

		ips, err := parseAddress(address)
		if err != nil {
			log.Warnf("Could not parse address %s: %s", address, err)

			continue
		}

		for _, ip := range ips {
			entries = append(entries, ipsets.IpsetEntry{
				Family:  ipsets.GetIPType(ip),
				Addr:    ip.String(),
				Timeout: timeout,
			})
		}
	}

	return entries, nil
}
