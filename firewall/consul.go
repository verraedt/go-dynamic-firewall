package firewall

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/hashicorp/consul/api"
	"golang.org/x/time/rate"

	log "github.com/sirupsen/logrus"
)

// ConsulListener listens to consul services and kvpairs.
type ConsulListener struct {
	Config     *api.Config
	RateLimit  *rate.Limiter
	sync.Mutex // Lock for rate limiter
}

// NewConsulListener provides a listener to consul updates.
func NewConsulListener(consulAddr string) (*ConsulListener, error) {
	config := api.DefaultConfig()

	if consulAddr != "" {
		config.Address = consulAddr
	}

	// Explicitly unset token
	config.Token = "anonymous"

	config.WaitTime = 5*time.Minute + 10*time.Second

	c := &ConsulListener{
		Config:    config,
		RateLimit: rate.NewLimiter(rate.Every(time.Minute), 20),
	}

	return c, nil
}

func (c *ConsulListener) Client() (*api.Client, error) {
	return api.NewClient(c.Config)
}

var ErrNoUpdate = errors.New("no new index")

// GetServices returns a list of services registered in consul, along with the change index.
func (c *ConsulListener) GetServices(ctx context.Context, requiredTags []string, index uint64) ([]*api.CatalogService, uint64, error) {
	queryoptions := &api.QueryOptions{}
	if index != 0 {
		queryoptions.WaitIndex = index
	}

	client, err := c.Client()
	if err != nil {
		return nil, 0, err
	}

	catalog := client.Catalog()

	services, meta, err := catalog.Services(queryoptions.WithContext(ctx))
	if err != nil {
		return nil, 0, err
	}

	if meta.LastIndex <= index {
		return nil, meta.LastIndex, ErrNoUpdate
	}

	log.Debugf("Received services: %+v", services)

	requiredTagsMap := make(map[string]bool)

	for _, tag := range requiredTags {
		requiredTagsMap[tag] = true
	}

	var (
		result []*api.CatalogService
		lookup []*api.CatalogService
		hasTag bool
	)

	for service, tags := range services {
		hasTag = false

		for _, tag := range tags {
			if _, ok := requiredTagsMap[tag]; ok {
				hasTag = true
			}
		}

		if hasTag {
			lookup, _, err = catalog.Service(service, "", (&api.QueryOptions{}).WithContext(ctx))
			if err != nil {
				return result, meta.LastIndex, err
			}

			result = append(result, lookup...)
		}
	}

	return result, meta.LastIndex, nil
}

// GetKVPairs returns consul kv pairs for a given prefix.
func (c *ConsulListener) GetKVPairs(ctx context.Context, prefix string, index uint64) ([]*api.KVPair, uint64, error) {
	queryoptions := &api.QueryOptions{}
	if index != 0 {
		queryoptions.WaitIndex = index
	}

	client, err := c.Client()
	if err != nil {
		return nil, 0, err
	}

	kv := client.KV()

	pairs, meta, err := kv.List(prefix, queryoptions.WithContext(ctx))
	if err != nil {
		return nil, 0, err
	}

	if meta.LastIndex <= index {
		return nil, meta.LastIndex, ErrNoUpdate
	}

	log.Debugf("Received pairs: %+v", pairs)

	return pairs, meta.LastIndex, nil
}

func (c *ConsulListener) rateLimit() error {
	c.Lock()
	defer c.Unlock()

	return c.RateLimit.Wait(context.Background())
}

// ListenServices listens for service changes.
func (c *ConsulListener) ListenServices(ctx context.Context, requiredTags []string) <-chan []*api.CatalogService {
	ch := make(chan []*api.CatalogService)

	go c.listen(ctx, func(ctx context.Context, index uint64) (uint64, error) {
		state, newIndex, err := c.GetServices(ctx, requiredTags, index)
		if err == nil {
			ch <- state
		}

		return newIndex, err
	}, func() {
		close(ch)
	})

	return ch
}

// ListenKVPairs listens for kv pairs changes.
func (c *ConsulListener) ListenKVPairs(ctx context.Context, prefix string) <-chan []*api.KVPair {
	ch := make(chan []*api.KVPair)

	go c.listen(ctx, func(ctx context.Context, index uint64) (uint64, error) {
		state, newIndex, err := c.GetKVPairs(ctx, prefix, index)
		if err == nil {
			ch <- state
		}

		return newIndex, err
	}, func() {
		close(ch)
	})

	return ch
}

type callback func(context.Context, uint64) (uint64, error)

func (c *ConsulListener) listen(ctx context.Context, cb callback, done func()) {
	var (
		index uint64
		errs  []error
	)

	defer done()

	for ctx.Err() == nil {
		if err := c.rateLimit(); err != nil {
			log.Error(err) // We don't expect an error from the rate limiter

			break
		}

		newIndex, err := cb(ctx, index)
		if err == nil {
			index = newIndex
			errs = nil

			continue
		}

		if errors.Is(err, ErrNoUpdate) {
			continue
		}

		if errors.Is(err, context.Canceled) {
			break
		}

		log.Warnf("Error occurred while listening for services: %s", err)

		errs = append(errs, err)

		// Dynamic timeout based on subsequent errors
		timeout := len(errs) * len(errs)
		if timeout > 625 {
			timeout = 625
		}

		log.Warnf("%d errors occurred, sleeping %d seconds.\n", len(errs), timeout)

		time.Sleep(time.Duration(timeout) * time.Second)
	}
}
