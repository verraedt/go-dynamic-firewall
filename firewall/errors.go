package firewall

import "errors"

// ErrNoIP is returned if the given ip cannot be parsed.
var ErrNoIP = errors.New("could not parse ip")

// ErrChannelClosed is returned if the channel is closed.
var ErrChannelClosed = errors.New("channel unexpectedly closed")
