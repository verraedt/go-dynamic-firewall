package firewall

import (
	"context"

	"github.com/hashicorp/consul/api"
	log "github.com/sirupsen/logrus"
	"gitlab.com/verraedt/go-dynamic-firewall/ipsets"
)

type Config struct {
	ConsulAddr string
	Loop       bool
}

type Firewall struct {
	Config
	ctx    context.Context
	cancel context.CancelFunc
	ipsets *ipsets.Control
}

func New(ctx context.Context, config Config) (*Firewall, error) {
	f := &Firewall{
		Config: config,
	}

	f.ctx, f.cancel = context.WithCancel(ctx)

	f.ipsets = ipsets.New()

	return f, nil
}

var RequiredTags = []string{"fw.internal", "fw.external"}

var KVIpsetPrefix = "ipsets/"

// Listen for input
func (f *Firewall) Listen() (<-chan Input, error) {
	consul, err := NewConsulListener(f.Config.ConsulAddr)
	if err != nil {
		return nil, err
	}

	servicesCh := consul.ListenServices(f.ctx, RequiredTags)
	kvpairsCh := consul.ListenKVPairs(f.ctx, KVIpsetPrefix)

	ch := make(chan Input)

	go f.dispatch(servicesCh, kvpairsCh, ch)

	return ch, nil
}

func (f *Firewall) dispatch(servicesCh <-chan []*api.CatalogService, kvpairsCh <-chan []*api.KVPair, out chan<- Input) {
	defer close(out)

	var input Input

	for {
		select {
		case v, ok := <-servicesCh:
			if !ok {
				return
			}

			log.Debugf("Received services: %v", v)

			input.Services = v

		case v, ok := <-kvpairsCh:
			if !ok {
				return
			}

			log.Debugf("Received kv pairs: %v", v)

			input.IpsetKVPairs = v
		}

		// Dispatch if all information is present
		if input.Services != nil && input.IpsetKVPairs != nil {
			log.Debug("Dispatching input")

			out <- input
		}
	}
}

func (f *Firewall) Run() error {
	ch, err := f.Listen()
	if err != nil {
		return err
	}

	for {
		input, ok := <-ch

		log.Debug("Received input")

		// Check whether context closed
		if err := f.ctx.Err(); err != nil {
			return err
		}

		// Check whether channel closed
		if !ok {
			return ErrChannelClosed
		}

		log.Debug("Checked context")

		// Compute output
		output, err := input.Compute()
		if err != nil {
			return err
		}

		log.Debug("Computed output, applying")

		// Apply output
		err = f.Apply(*output)
		if err != nil {
			return err
		}

		if !f.Config.Loop {
			return nil
		}
	}
}

func (f *Firewall) Apply(state Output) error {
	if err := f.ipsets.Set("DYNAMIC-INTERNAL", "hash:ip,port", 0).Update(state.DynamicInternal); err != nil {
		return err
	}

	if err := f.ipsets.Set("DYNAMIC-EXTERNAL", "hash:ip,port", 0).Update(state.DynamicExternal); err != nil {
		return err
	}

	for ipsetName, entries := range state.Ipsets {
		if err := f.ipsets.Set(ipsetName, "hash:net", 300).Update(entries); err != nil {
			return err
		}
	}

	return nil
}

func (f *Firewall) Close() {
	f.cancel()
}
